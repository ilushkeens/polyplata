# polyplata change log

## Version 1.0.0 under development

- New: Added all page templates
- New: Added Montserrat and Jost fonts
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init August 5, 2022
