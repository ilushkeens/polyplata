'use strict'

document.addEventListener('DOMContentLoaded', () => {
  // Плавно прокручивает к якорю
  const anchors = document.querySelectorAll('.scroll-to')

  anchors.forEach(anchor => {
    anchor.addEventListener('click', function (event) {
      event.preventDefault()

      let sectionId = anchor.getAttribute('href').substring(1)

      document.getElementById(sectionId).scrollIntoView({
        behavior: 'smooth',
        block: 'start'
      })
    })
  })

  // Открывает вкладки
  const links = document.querySelectorAll('.open-tab'),
        tabs = document.querySelectorAll('.region-info-tabs__item')

  links.forEach(link => {
    link.addEventListener('click', function (event) {
      event.preventDefault()

      let tabId = link.getAttribute('href').substring(1)

      tabs.forEach(tab => {
        tab.classList.remove('open')
      })
      document.getElementById(tabId).classList.add('open')

      links.forEach(link => link.parentElement.classList.remove('current'))
      link.parentElement.classList.add('current')
    })
  })

  // Анимация
  let objectWrappers = document.querySelectorAll('.object-wrapper')

  objectWrappers.forEach(objectWrapper => {
    let objects = objectWrapper.querySelectorAll('.object')

    objects.forEach(object => {
      object.classList.remove('object-animation')

      let observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
          if (typeof getCurrentAnimationPreference === 'function' && !getCurrentAnimationPreference()) {
            return
          }

          if (entry.isIntersecting) {
            object.classList.add('object-animation')
            return
          }

          object.classList.remove('object-animation')
        })
      })

      observer.observe(objectWrapper)
    })
  })
})
